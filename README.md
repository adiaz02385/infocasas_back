### Requirements

    1. PHP version upto 7.4   
    2. Laravel version  8.0   

### Installation

    1. Clone the project using git clone   
    2. Copy a fresh .env file from laravel github    
    3. Update .env file by adding database information like: DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD.  
    4. Go to project root folder.Open git bash or terminal and run "composer install"       
    5. Run "php artisan key:generate" in the terminal    
    6. Run "php artisan migrate --seed"  
    9. Run "php artisan serve"
    10.Visit http://127.0.0.1:8000/api/task/ (show tasks list on browser)
