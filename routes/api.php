<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TasksController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors'], function() {
	Route::get('/task/status/{status}', [TasksController::class, 'index']);
	Route::get('/task/buscar/{name?}', [TasksController::class, 'index']);
	Route::get('/task/difficulty/{difficulty}', [TasksController::class, 'index']);
	Route::get('/task', [TasksController::class, 'index'])->name('task.index');
	Route::post('/task', [TasksController::class, 'store'])->name('task.store');
	Route::get('/task/{task}', [TasksController::class, 'show'])->name('task.show');
	Route::post('/task/update/{task}', [TasksController::class, 'update'])->name('task.update');
	Route::post('/task/delete/{task}', [TasksController::class, 'destroy'])->name('task.destroy');
});