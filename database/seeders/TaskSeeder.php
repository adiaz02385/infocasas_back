<?php

namespace Database\Seeders;

use App\Models\Task;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tasks')->delete();
        
        $tasks = [
            0 => [
                'name'       => 'Formatear Computador',
                'difficulty' => 'medium',
                'completed'  => true,
            ],
            1 => [
                'name'       => 'Respaldo y migración de datos',
                'difficulty' => 'hard',
                'completed'  => true,
            ],
            2 => [
                'name'       => 'Instalar S.O',
                'difficulty' => 'easy',
                'completed'  => false,
            ],
            3 => [
                'name'       => 'Configurar VPN',
                'difficulty' => 'easy',
                'completed'  => false,
            ],
            4 => [
                'name'       => 'Servidor de Impresión',
                'difficulty' => 'medium',
                'completed'  => false,
            ],
        ];

        foreach ($tasks as $task) {
            Task::create([
                'name'       => $task['name'],
                'difficulty' => $task['difficulty'],
                'completed'  => $task['completed']
            ]);
        }
    }
}
