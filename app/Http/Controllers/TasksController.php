<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $task = Task::query();
        if ($request->status == 'completed') {
            $task = $task->completed();
        }
        if ($request->status == 'pending') {
            $task = $task->pending();
        }
        if (!empty($request->name)) {
            $task = $task->where('name', 'LIKE', '%'.$request->name.'%');
        }
        if ($request->difficulty) {
            $task = $task->where('difficulty', $request->difficulty);
        }
        $task = $task->orderBy('updated_at', 'DESC')->get();
        return response()->json(['status' => 200, 'task' => $task->toArray()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

        $newTask = Task::create([
            'name'       => $request->name,
            'difficulty' => $request->difficulty,
            'completed' => ($request->completed == 'on') ? 1 : 0,
        ]);
        if ($newTask) {
            return response()->json(["status" => 200]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        return response()->json(['status' => 200, 'task' => $task]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

        $task->name       = $request->name;
        $task->difficulty = $request->difficulty;
        $task->completed  = $request->completed;
        if ($task->save()) {
            return response()->json(["status" => 200]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        if ($task->delete()) {
            return response()->json(["status" => 200]);
        }
    }
}
